from collections import defaultdict
from copy import deepcopy

class site:
    def __init__(self, site_num):
        self.site_num = site_num
        self.resource = defaultdict(list)
        self.lock_table = defaultdict(set)
        self.up = True
        self.sitedowntime = []
        self.updated = {}
        self.temp_write = {}

    def add_resource(self, variable, value):
        """
        Add a variable to the current site
        input: variable: string
               value: int
        output: void
        """
        self.resource[variable].append((0, value))
        self.updated[variable] = True

    def read(self, tick, transaction, variable, is_replicated):
        if not self.up:
            return None
        if is_replicated and not self.updated[variable]:
            return None

        is_previously_written = False
        value = None
        for lock_trans, lock in self.lock_table[variable]:
            if lock == 'W':
                if lock_trans != transaction.name:
                    return None
                else:
                    is_previously_written = True

        _, value = self.resource[variable][-1]
        if is_previously_written:
            _, value = self.temp_write[variable]

        transaction.add_accessed_site(self, tick)
        self.lock_table[variable].add((transaction.name, 'R'))

        return value

    def readRO(self, tick, transaction, variable):
        if not self.up:
            return None, None

        for prev_version, prev_value in reversed(self.resource[variable]):
            if prev_version <= transaction.start:
                transaction.add_accessed_site(self, tick)
                return prev_version, prev_value

        return None, None

    def is_writable(self, transaction, variable):
        for lock_trans, lock in self.lock_table[variable]:
            if lock_trans != transaction.name:
                return False
        return True

    def has_been_up(self, start, end):
        for down_time in reversed(self.sitedowntime):
            if down_time < start:
                break
            if down_time > start and down_time < end:
                return False
        return True

    def write(self, tick, transaction, variable, value):
        if not self.up:
            return
        self.temp_write[variable] = (transaction.name, value)
        self.lock_table[variable].add((transaction.name, 'W'))
        transaction.add_accessed_site(self, tick)

    def abort(self, transaction_name):
        # both lock_table and temp_write are empty when site is down
        for variable, value in list(self.temp_write.items()):
            temp_trans_name, _ = value
            if transaction_name == temp_trans_name:
                del self.temp_write[variable]
        self.removefromlocktable(transaction_name)

    def commit(self, transaction_name, cur_tick):
        for variable, value in self.temp_write.items():
            temp_trans_name, temp_value = value
            if transaction_name == temp_trans_name:
                self.resource[variable].append((cur_tick, temp_value))
                self.updated[variable] = True

        self.removefromlocktable(transaction_name)

    def removefromlocktable(self, transaction_name):
        for variable, lock_set in self.lock_table.items():
            for lock_trans in set(lock_set):
                lock_trans_name, _ = lock_trans
                if transaction_name == lock_trans_name:
                    lock_set.remove(lock_trans)

    def fail(self, cur_tick):
        self.lock_table = defaultdict(set)
        self.temp_write = {}
        self.up = False
        for var in self.updated:
            self.updated[var] = False
        self.sitedowntime.append(cur_tick)

    def recover(self):
        self.up = True

    def dump_site(self, dump_dict):
        if not self.up:
            return
        for variable in self.resource:
            dump_dict[variable] = self.resource[variable][-1][1]

    def dump_variable(self, variable):
        if not self.up:
            return
        if variable in self.resource:
            return str(self.resource[variable][-1][1])
        else:
            return "N"
