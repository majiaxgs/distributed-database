from collections import defaultdict, deque
from Transaction import transaction
import re

class transaction_manager:
    def __init__(self, sites):
        self.transaction_graph = defaultdict(set)
        self.data_managers = sites
        self.wait_list = []
        self.ROtransactions = {}
        self.RWtransactions = {}
        self.variables = set()
        self.tick = 0

        for site in sites:
            for variable in site.resource.keys():
                self.variables.add(variable)

    def handle_request(self, request):
        self.retry_wait_list()

        self.tick += 1

        self.parse_outer_request(request)

        while self.cycle_detection():
            pass

    def parse_outer_request(self, request):
        request_type, content = request[:request.find(")")].replace(" ", "").split('(')

        if request_type == "begin":
            self.RWtransactions[content] = transaction(content, self.tick)

        elif request_type == "beginRO":
            self.ROtransactions[content] = transaction(content, self.tick)

        elif request_type == "R":
            transaction_name, variable = content.replace(" ", "").split(",")
            if transaction_name in self.RWtransactions:
                self.handle_read(self.RWtransactions[transaction_name], variable)
            elif transaction_name in self.ROtransactions:
                self.handle_ROread(self.ROtransactions[transaction_name], variable)
            else:
                raise ValueError("Operation happend before transaction begins")

        elif request_type == "W":
            transaction_name, variable, value = content.replace(" ", "").split(",")
            self.handle_write(self.RWtransactions[transaction_name], variable, int(value))

        elif request_type == "end":
            self.handle_end(content)

        elif request_type == "fail":
            site = self.data_managers[int(content) - 1]
            self.handle_fail(site)

        elif request_type == "recover":
            site = self.data_managers[int(content) - 1]
            self.handle_recover(site)

        elif request_type == "dump":
            if content.startswith('x'):
                self.print_firstline_site()
                self.dump_variable(content)
            elif len(content) > 0:
                self.print_firstline_variable()
                self.dump_site(content)
            else:
                self.print_firstline_variable()
                self.dump_all()
        else:
            raise ValueError("Not valid request:" + request)

    def translate(self, inner_request):
        # handle requests in the queue first
        request_type = inner_request[0]

        if request_type == "R":
            self.handle_read(inner_request[1], inner_request[2])

        elif request_type == "RO":
            self.handle_ROread(inner_request[1], inner_request[2])

        elif request_type == "W":
            self.handle_write(inner_request[1], inner_request[2], inner_request[3])

        else:
            raise ValueError("Not valid request: " + inner_request)

    def handle_read(self, transaction, variable):
        index_num = int(variable[1:])
        value = None
        if index_num % 2 == 0:
            for dm in self.data_managers:
                value = dm.read(self.tick, transaction, variable, True)
                if value != None:
                    break
        else:
            dm = self.data_managers[index_num % 10]
            value = dm.read(self.tick, transaction, variable, False)

        if value == None:
            self.transaction_graph[transaction.name].add(variable)
            self.wait_list.append(("R", transaction, variable))
            return
        else:
            self.transaction_graph[variable].add(transaction.name)

        print("R", transaction.name, variable, value)

    def handle_ROread(self, transaction, variable):
        index_num = int(variable[1:])
        latest_value = None
        latest_version = -1
        if index_num % 2 == 0:
            for dm in self.data_managers:
                version, value = dm.readRO(self.tick, transaction, variable)
                if value != None and version > latest_version:
                    latest_value = value
                    latest_version = version
        else:
            dm = self.data_managers[index_num % 10]
            _, latest_value = dm.readRO(self.tick, transaction, variable)

        if latest_value == None:
            self.wait_list.append(("RO", transaction, variable))
            return
        print("RO", transaction.name, variable, latest_value)

    def available_value(self, transaction, variable, version):
        index_num = int(variable[1:])
        value = None
        if index_num % 2 == 0:
            for dm in self.data_managers:
                value = dm.read(self.tick, transaction, variable, version, True)
                if value != None:
                    break
        else:
            dm = self.data_managers[index_num % 10]
            value = dm.read(self.tick, transaction, variable, version, False)

        return value

    def handle_write(self, transaction, variable, value):
        index_num = int(variable[1:])

        if index_num % 2 == 0:
            iswritable = None

            for dm in self.data_managers:
                if iswritable == None:
                    iswritable = dm.is_writable(transaction, variable)
                else:
                    iswritable = iswritable and dm.is_writable(transaction, variable)

            if iswritable == True:
                for dm in self.data_managers:
                    dm.write(self.tick, transaction, variable, value)
                self.transaction_graph[variable].add(transaction.name)
                return
        else:
            dm = self.data_managers[index_num % 10]
            if dm.is_writable(transaction, variable):
                dm.write(self.tick, transaction, variable, value)
                self.transaction_graph[variable].add(transaction.name)
                return

        self.transaction_graph[transaction.name].add(variable)
        self.wait_list.append(("W", transaction, variable, value))

    def cycle_detection(self):
        # print("DEBUG: transaction_graph:", list(self.transaction_graph.items()))
        recStack = defaultdict(str)
        visited = set()
        for node in list(self.transaction_graph.keys()):
            if node not in visited and node.startswith('T'):
                if self.cycle_detection_helper(node, visited, recStack):
                    ### cycle detected; abort the youngest transaction
                    transaction_set = set()
                    youngest = self.RWtransactions[node]
                    for k in recStack.keys():
                        if k.startswith('T'):
                            transaction_set.add(k)
                            if self.RWtransactions[k].start > youngest.start:
                                youngest = self.RWtransactions[k]

                    # If it is a self cycle, the transaction should not be aborted
                    if len(transaction_set) > 1:
                        print("Deadlock detected; Transaction", youngest.name, "is aborted.")
                        self.abort_transaction(youngest)
                        return True
                    recStack.clear()

        return False

    def cycle_detection_helper(self, node, visited, recStack):
        visited.add(node)
        for child in self.transaction_graph[node]:
            recStack[node] = child
            if child not in visited:
                if self.cycle_detection_helper(child, visited, recStack):
                    return True
            elif child in recStack:
                return True

        if node in recStack:
            del recStack[node]

        return False

    def handle_end(self, transaction_name):
        if transaction_name in self.RWtransactions:
            transaction = self.RWtransactions[transaction_name]
            for site, tick in transaction.accessed_sites:
                if not site.has_been_up(tick, self.tick):
                    self.abort_transaction(transaction)
                    print("%s aborted because site %d failed before the transaction ends." % (transaction_name, site.site_num))
                    return
            self.commit_transaction(transaction)
            self.remove_transaction(transaction)
            print(transaction_name, "commmits")

    def retry_wait_list(self):
        # Remove the influence of the wait list in transaction graph
        for request in self.wait_list:
            transaction = request[1]
            if transaction.name in self.transaction_graph:
                del self.transaction_graph[transaction.name]

        # Handle waiting operations according to the start time
        copy_wait_list = list(self.wait_list)
        self.wait_list = []
        for request in copy_wait_list:
            self.translate(request)

    def abort_transaction(self, transaction):
        for site, _ in transaction.accessed_sites:
            site.abort(transaction.name)
        self.remove_transaction(transaction)

    def commit_transaction(self, transaction):
        for site, _ in transaction.accessed_sites:
            site.commit(transaction.name, self.tick)

    def remove_transaction(self, transaction):
        # remove from transaction graph
        if transaction.name in self.transaction_graph:
            del self.transaction_graph[transaction.name]
        for _, v in self.transaction_graph.items():
            if transaction.name in v:
                v.remove(transaction.name)

        # remove from wait queue
        new_list = []
        for operation in self.wait_list:
            if operation[1].name != transaction.name:
                new_list.append(operation)
        self.wait_list = new_list

        # remove from transaction holders
        if transaction.name in self.RWtransactions:
            del self.RWtransactions[transaction.name]
        elif transaction.name in self.ROtransactions:
            del self.ROtransactions[transaction.name]

    def handle_fail(self, site):
        site.fail(self.tick)

    def handle_recover(self, site):
        site.recover()

    def print_firstline_variable(self):
        print("\t", end="")
        for i in range(1, 21):
            print("x"+str(i)+"\t", end="")
        print()

    def print_firstline_site(self):
        print("\t", end="")
        for i in range(1, 11):
            print("site"+str(i)+"\t", end="")
        print()

    def dump_site(self, site_num):
        dump_dict = {}
        self.data_managers[int(site_num) - 1].dump_site(dump_dict)
        print("site"+site_num+"\t", end="")
        for i in range(1, 21):
            var = "x"+str(i)
            if var in dump_dict:
                print(str(dump_dict[var])+"\t", end="")
            else:
                print("N"+"\t", end="")
        print()

    def dump_variable(self, variable):
        print(variable+"\t", end="")
        index_num = int(variable[1:])
        if index_num % 2 == 0:
            for idx, dm in enumerate(self.data_managers):
                print(dm.dump_variable(variable)+"\t", end="")
        print()

    def dump_all(self):
        for i in range(1, len(self.data_managers)+1):
            self.dump_site(str(i))
