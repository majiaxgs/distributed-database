# NYU Fall 2017 - Advanced Database System Final Project: Distributed Database Management System

Jinglin Wang (jw4716@nyu.edu)

Yicong Tao (yt1182@nyu.edu)

## Prerequisites
* Python 3.5.2 or up

## How to run the test

First, `git clone` the project into your local disk.

In the repository directory, simply run the test script `run.sh` in a bash environment.

All test cases in `test/in` will be executed and the result will be stored in `test/out`.

You may add your own test cases into `test/in` (the filename should be a subsequent number)
or you may manually run the test by issuing the following command:

`python3 main.py < [test input file]`

The output will be displayed on the screen.
