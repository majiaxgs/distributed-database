class transaction:
    def __init__(self, name, start):
        self.name = name
        self.start = start
        self.accessed_sites = []

    def add_accessed_site(self, site, tick):
        self.accessed_sites.append((site, tick))
