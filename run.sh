#!/bin/bash

mkdir -p test/out

num="$(ls -1 test/in | wc -l)"

for i in $(seq 1 $num)
do
    testin="test/in/$i.in"
    testout="test/out/$i.out"
    echo "Testing $testin"
    python3 main.py <$testin >$testout
done

