import sys
from Transaction_manager import transaction_manager
from Site import site

sites = []
for i in range(10):
    sites.append(site(i + 1))

for x in range(1, 21):
    if x % 2 == 1:
        sites[x % 10].add_resource('x' + str(x), 10 * x)
    else:
        for site in sites:
            site.add_resource('x' + str(x), 10 * x)

tm = transaction_manager(sites)

for line in sys.stdin:
    tm.handle_request(line)
